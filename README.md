Xenko Voxelscape
==============
An open source voxel engine using C# and the Xenko game engine.
Still in the early stages of development and no longer being actively worked on.
Released under the MIT License.

Only demonstrates procedurally generating sky islands. No runtime editing of voxels and no physical collisions.

[Click here](https://www.youtube.com/watch?v=whJOtux2DrQ) for a video demonstration of the voxel engine.